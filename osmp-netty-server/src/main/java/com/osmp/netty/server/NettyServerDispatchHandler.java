 /*   
 * Project: OSMP
 * FileName: NettyServerDispatchHandler.java
 * version: V1.0
 */
package com.osmp.netty.server;

import java.net.InetSocketAddress;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.osmp.intf.define.config.FrameConst;
import com.osmp.intf.define.model.InvocationDefine;
import com.osmp.intf.define.model.Parameter;
import com.osmp.intf.define.model.ServiceContext;
import com.osmp.intf.define.server.Request;
import com.osmp.intf.define.server.Response;
import com.osmp.netty.service.NettyServiceInvocation;
import com.osmp.netty.service.ServiceFactoryManager;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 下午3:01:51上午10:51:30
 */
@Sharable
@Component
public class NettyServerDispatchHandler extends SimpleChannelInboundHandler<Request>{

	private Logger logger = LoggerFactory.getLogger(NettyServerDispatchHandler.class);
	private static final String SUCC_CODE = "0";
	private static final String ERROR_CODE = "1";
	
	@Resource
	private ServiceFactoryManager serviceFactory;
	
	@Override
	protected void messageReceived(ChannelHandlerContext ctx, Request request) throws Exception {
		InetSocketAddress socketAddress = (InetSocketAddress) ctx.pipeline().channel().remoteAddress();
		String remoteIp = socketAddress.getAddress().getHostAddress();
		if(request.getSource() != null){
			request.getSource().put(FrameConst.CLIENT_IP, remoteIp);
		}
		logger.info("==========received:("+remoteIp+") request:("+request.toString()+")");
		Response response = execute(request);
		logger.info("==========return:("+remoteIp+") result:("+response.toString()+")");
		ctx.writeAndFlush(response);
	}
	
	private Response execute(Request request){
		if(StringUtils.isEmpty(request.getService())){
			return new Response(request.getMsgId(),ERROR_CODE,"服务名不能为空","");
		}
		InvocationDefine invocation = serviceFactory.getInvocationDefine(request.getService());
		if(null == invocation){
			return new Response(request.getMsgId(), ERROR_CODE, "未找到服务"+request.getService(), "");
		}
		
		ServiceContext serviceContext = new ServiceContext(request.getSource(), request.getService(), "");
		serviceContext.setParametor(new Parameter(request.getSource(), null, (Map<String, String>) request.getParameter()));
		
		NettyServiceInvocation invoke = new NettyServiceInvocation(invocation.getInterceptors(), invocation.getDataService(), serviceContext);
		Object result = invoke.process();
		String json = result == null ? "" : JSONObject.toJSONString(result);
		if(invoke.isError()){
			return new Response(request.getMsgId(),ERROR_CODE,"",json);
		}
		return new Response(request.getMsgId(),SUCC_CODE,"",json);
	}

	public void setServiceFactory(ServiceFactoryManager serviceFactory) {
		this.serviceFactory = serviceFactory;
	}

}
