package com.osmp.web.system.servers.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.servers.entity.Servers;

public interface ServersMapper extends BaseMapper<Servers> {

}
