 /*   
 * Project: OSMP
 * FileName: Server.java
 * version: V1.0
 */
package com.osmp.intf.define.server;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 上午11:13:44上午10:51:30
 */
public interface Server {
	
	long SYSTEM_MESSAGE_ID = -1L;
	
	void start();
	
	void stop();

}
